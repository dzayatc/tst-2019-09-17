function getPhoneByCity(city) {
    const phones = getPhones();

    if (phones.hasOwnProperty(city)) {
        return phones[city];
    }
    return null;
}

function getPhones() {
    return {
        'Калуга': '+7 483 123-00-00',
        'Брянск': '+7 482 123-00-00',
        'Москва': '+7 495 123-00-00',
    }
}