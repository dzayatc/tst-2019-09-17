<?php
/**
 * Class Second
 * 2) (на PHP) Задан массив 5 на 7, который заполнен случайными числами от 1 до 1000. Нужно вывести массив, сумму чисел по строкам и сумму чисел по колонкам.
 */

class Second
{

    /** @var array */
    private $array = [];

    public function __construct()
    {
        $this->fillArray();
    }

    public function run()
    {
        echo "\n\n";

        $columnsSum = [];

        for ($i = 0; $i < 5; $i++) {
            $rowSum = 0;
            for ($j = 0; $j < 7; $j++) {
                printf('%6d', $this->array[$i][$j]);
                $rowSum += $this->array[$i][$j];
                if (!array_key_exists($j, $columnsSum)) {
                    $columnsSum[$j] = 0;
                }
                $columnsSum[$j] += $this->array[$i][$j];
            }
            printf(" | %6d\n", $rowSum);
        }
        echo "---------------------------------------------------\n";
        // Вывод суммы чисел по колонкам
        for ($j = 0; $j < 7; $j++) {
            printf('%6d', $columnsSum[$j]);
        }
        echo "\n\n";
    }

    public function fillArray()
    {
        $this->array = [];
        for ($i = 0; $i < 5; $i++) {
            $this->array[] = [];
            for ($j = 0; $j < 7; $j++) {
                $this->array[$i][] = mt_rand(1, 1000);
            }
        }
    }

}