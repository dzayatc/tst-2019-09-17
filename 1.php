<?php
/**
 * 1) (на PHP) вывести массив от 1 до 100 - лесенкой
1
2 3
4 5 6
…
 */

// Вариант, если не считать "массив"требованием к реализации
run();

// Вариант с буквальным пониманием слова "массив"
//run2();

/**
 * Вывод от 1 до 100  лесенкой
 */
function run()
{
    echo "\n";
    $strLength = 1;
    $curInStr = 0;

    for ($i = 1; $i <= 100; $i++) {
        echo $i . ' ';
        $curInStr++;
        if ($curInStr >= $strLength) {
            echo "\n";
            $strLength++;
            $curInStr = 0;
        }
    }
    echo "\n";
}

/**
 * Вывод предварительно сформированного массива размерностью 100  лесенкой
 */
function run2()
{
    echo "\n";
    $array = generateArray();
    showArray($array);
    echo "\n";
}

/**
 * @return array
 */
function generateArray()
{
    $result = [];
    for ($i = 1; $i <= 100; $i++) {
        $result[] = $i;
    }
    return $result;
}

/**
 * Выводит масси "лесенкой"
 * @param array $array
 */
function showArray(array $array)
{
    $strLength = 1;
    $curInStr = 0;

    foreach ($array as $item) {
        echo $item . ' ';
        $curInStr++;
        if ($curInStr >= $strLength) {
            echo "\n";
            $strLength++;
            $curInStr = 0;
        }
    }
}
